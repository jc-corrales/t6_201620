package taller.estructuras;

import java.util.ArrayList;


public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private ArrayList<NodoHash<K, V>>[] array;
	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		array = new ArrayList[1024];
		for(int i = 0; i < capacidad; i++)
		{
			array[i] = new ArrayList<NodoHash<K, V>>();
		}
		capacidad = 1024;
		count = 0;
		factorCargaMax = ((75*1024)/100);
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax)
	{
		array = new ArrayList[capacidad];
		count = 0;
		for(int i = 0; i < capacidad; i++)
		{
			array[i] = new ArrayList<NodoHash<K, V>>();
		}
		this.capacidad = capacidad;
		this.factorCargaMax = factorCargaMax;
		//TODO: Inicialice la tabla con los valores dados por parametro

	}

	public void put(K llave, V valor)
	{
//		System.out.println("Agregando: " + valor + "Llave: " + llave);
		int pos = hash(llave);
		NodoHash<K, V> nuevo = new NodoHash<K, V>(llave, valor);
		array[pos].add(nuevo);
		if(array[pos].size() == 1)
		{
			count++;
		}	
		reHash();
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
	}
	/**
	 * Método que obtiene el valor asociado a una llave dada.
	 * @param llave llave del valor a buscar.
	 * @return Valor del elemento buscado.
	 */
	public V get(K llave)
	{
		V elemento = null;
		NodoHash<K, V> nodo = null;
		int pos = hash(llave);
		if(pos < 0)
		{
			pos = Math.abs(pos);
		}
		for(int i = 0; i < array[pos].size(); i++)
		{
			if(array[pos].get(i).getLlave().equals(llave))
			{
				nodo = array[pos].get(i);
			}
		}
		elemento = nodo.getValor();
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		return elemento;
	}
	
	/**
	 * Método que elimina el valor buscado y retorna el elemento.
	 * @param llave
	 * @return V elemento.
	 */
	public V delete(K llave)
	{
		V elemento = null;
		NodoHash<K, V> nodo = null;
		int pos = hash(llave);
		for(int i = 0; i < array[pos].size(); i++)
		{
			if(array[pos].get(i).getLlave().equals(llave))
			{
				nodo = array[pos].get(i);
				array[pos].remove(i);
			}
		}
		elemento = nodo.getValor();
		if(array[pos].size() == 0)
		{
			count--;
		}
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		return elemento;
	}
	
	/**
	 * Método que retorna la cantidad de objetos actuales.
	 * @return
	 */
	public int size()
	{
		return count;
	}
	
	//Hash
	/**
	 * Método que retorna la posición de una tupla usando su llave.
	 * @param llave de la tupla.
	 * @return posición en la tabla de hash.
	 */
	private int hash(K llave)
	{
		int pos = llave.hashCode()%capacidad;
		if(pos < 0)
		{
			pos = Math.abs(pos);
		}
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		return pos;
	}
	
	private void calcularFactorDeCargaActual()
	{
		factorCarga = (count*100)/capacidad;
	}
	/**
	 * Método que verifica si el rehash es necesario, y de serlo, lo realiza.
	 */
	private void reHash()
	{
		calcularFactorDeCargaActual();
		if(factorCarga >= factorCargaMax)
		{
			ArrayList<NodoHash<K, V>>[] nuevo = new ArrayList[capacidad*2];
			int capacidadVieja = capacidad;
			for(int i = 0; i < capacidad; i++)
			{
				nuevo[i] = new ArrayList<NodoHash<K, V>>();
			}
			capacidad = capacidad*2;
			for(int i = 0; i < capacidadVieja; i++)
			{
				for(int j = 0; j < array[i].size(); j++)
				{
					NodoHash<K, V> elemento = array[i].remove(j);
					int pos = hash(elemento.getLlave());
					nuevo[pos].add(elemento);
				}
			}
			array = nuevo;
		}
	}
		//TODO: Permita que la tabla sea dinamica

}