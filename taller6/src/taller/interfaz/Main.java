package taller.interfaz;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import taller.estructuras.TablaHash;
import taller.estructuras.TablaHashNotUnique;


public class Main
{

	private static String rutaEntrada = "./data/ciudadLondres.csv";
	
	private static String rutaCoordenadas = "./data/coordenadas.txt";

	/**
	 * Directorio original.
	 */
	private static TablaHash<Integer, String> directorio;
	/**
	 * Directorio con coorenadas.
	 */
	private static TablaHashNotUnique<String, String> directorio2;
	/**
	 * Directorio con Apellidos como llave.
	 */
	private static TablaHashNotUnique<String, Integer> directorio3;
	
	public static void main(String[] args) {
		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();

			//TODO: Inicialice el directorio t
			directorio = new TablaHash<>(4000000, 75);
			directorio2 = new TablaHashNotUnique<>(4000000, 75);
			directorio3 = new TablaHashNotUnique<>(4000000, 75);
			int i = 0;
			entrada = br.readLine();
			while (entrada != null)
			{
				String[] datos = entrada.split(",");
				String nombres = datos[0] + " " + datos[1];
				int llave = Integer.parseInt(datos[2]);
				directorio.put(llave, nombres);
				directorio3.put(datos[1], llave);
				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información
				++i;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
			escritor();
			
			BufferedReader br1 = new BufferedReader(new FileReader(new File(rutaCoordenadas)));
			String entrada1 = br1.readLine();
			int j = 0;
			while(entrada1 != null)
			{
				String[] datos = entrada1.split(",");
				String nombres = datos[0];
				String llave = datos[1];
				directorio2.put(llave, nombres);
				j++;
				
				if (++j%500000 == 0)
					System.out.println(j+" entradas de coordenadas...");
				entrada1 = br1.readLine();
			}
			System.out.println(j + " entradas cargadas en total.");
			br1.close();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por apellido");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				switch (in) {
				case "1":
					System.out.println("Escriba en el siguiente formato:");
					System.out.println("Número telefónico, Nombre");
					String in2 = br.readLine();
					String[] datos = in2.split(",");
					directorio.put(Integer.parseInt(datos[0]), datos[1]);
					in2=null;
					//Agregar ciudadano a la lista de emergencia
					break;
				case "2":
					System.out.println("Introduci un número de 7 dígitos.");
					String in3 = br.readLine();
					int llave = Integer.parseInt(in3);
					System.out.println(directorio.get(llave));
					in3 = null;
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					break;
				case "3":
					System.out.println("Con el Apellido introducido, se devolverán todos los números telefónicos asociados con dicho Apellido.");
					System.out.println("Favor introducir un apellido: ");
					String in4 = br.readLine();
					ArrayList<Integer> lista = directorio3.get(in4);
					for(int i = 0; i < lista.size(); i++)
					{
						System.out.println(lista.get(i));
					}
					in4 = null;
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre
					break;
				case "4":
					System.out.println("Favor escribir la coordenadas así: (grados°minutos'segundos'')");
					String in5 = br.readLine();
					System.out.println(directorio2.get(in5));
					
					in5= null;
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual
					break;
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	/**
	 * algo
	 */
	public static void escritor()
	{
		
		
		try
		{	
			File file = new File(rutaCoordenadas);
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			
			
			String content = "";
			BufferedReader br1 = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br1.readLine();
			Random generador = new Random(89);
			//TODO: Inicialice el directorio t
			int i = 0;
			entrada = br1.readLine();
			while (entrada != null)
			{
				
				double grados = generador.nextDouble();
				grados += generador.nextInt(90);
				double minutos = generador.nextDouble();
				minutos += generador.nextInt(90);
				double segundos = generador.nextDouble();
				segundos += generador.nextInt(90);
				String[] datos = entrada.split(",");
				String nombres = datos[0] + " " + datos[1];
				content = nombres + "," + grados + "°" + minutos + "'" + segundos + "''";
				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información
				++i;
//				System.out.println("procesamiento " + i + nombres);
//				System.out.println(nombres + " Coordenadas: " + grados + "°" + minutos + "'" + segundos + "''");
				bw.write(content);
				bw.newLine();
				
				if (++i%50000 == 0)
					System.out.println(i+" entradas...");

				entrada = br1.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br1.close();
			bw.close();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}