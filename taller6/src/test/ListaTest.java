package test;

import taller.estructuras.Lista;
import junit.framework.TestCase;

/**
 * Clase para verificar el funcionamiento de la clase Lista.
 * @author JuanCarlos
 *
 */
public class ListaTest extends TestCase
{
	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------
	/**
	 * Lista de números.
	 */
	private Lista<Integer> numeros;
	/**
	 * Lista de Palabras
	 */
	private Lista<String> palabras;

	// -------------------------------------------------------------
	// Métodos
	// -------------------------------------------------------------

	public void setupEscenario1()
	{
		numeros = new Lista<Integer>();
		palabras = new Lista<String>();
	}
	
	public void setupEscenario2()
	{
		setupEscenario1();
		for(Integer i= 100; i < 10000; i = i+100)
		{
			numeros.add(i);
		}
	}
	/**
	 * Prueba del método Add.
	 */
	public void testAdd()
	{
		setupEscenario1();
		numeros.add(0);
		numeros.add(1);
		numeros.add(2);
		numeros.add(3);
		Integer pos0 = 0;
		Integer pos1 = 1;
		Integer pos2 = 2;
		Integer pos3 = 3;
		assertEquals("El tamaño debería ser 4", 4, numeros.size());
		assertEquals("Elemento en posición 0 debe ser 0",  pos0 , numeros.get(0));
		assertEquals("Elemento en posición 1 debe ser 1",  pos1 , numeros.get(1));
		assertEquals("Elemento en posición 2 debe ser 2",  pos2 , numeros.get(2));
		assertEquals("Elemento en posición 3 debe ser 3",  pos3 , numeros.get(3));
	}
	/**
	 * Prueba del método delete.
	 */
	public void testDelete()
	{
		setupEscenario1();
		numeros.add(0);
		numeros.add(1);
		numeros.add(2);
		numeros.add(3);
		numeros.delete();
		numeros.delete();
		assertEquals("El tamaño debería ser 2", 2, numeros.size());
		try
		{
			assertNull(numeros.get(3));
		}
		catch (Exception e)
		{
			assertNotNull("Debería haber arrojado excepción.", e);
		}
		try
		{
			assertNull(numeros.get(4));
		}
		catch (Exception e)
		{
			assertNotNull("Debería haber arrojado excepción.", e);
		}
	}
	/**
	 * Prueba del método get.
	 */
	public void testGet()
	{
		setupEscenario2();
		Integer num100 = 100;
		Integer num200 = 200;
		Integer num300 = 300;
		assertEquals("Elemento debería ser 100", num100, numeros.get(0));
		assertEquals("Elemento debería ser 200", num200, numeros.get(1));
		assertEquals("Elemento debería ser 300", num300, numeros.get(2));
	}
	/**
	 * Prueba del método remove.
	 */
	public void testRemove()
	{
		setupEscenario1();
		numeros.add(10);
		numeros.add(100);
		numeros.add(200);
		numeros.add(300);
		numeros.remove(0);
		numeros.remove(0);
		Integer num200 = 200;
		Integer num300 = 300;
		assertEquals("El tamaño debería ser 2", 2, numeros.size());
		try
		{
			assertNull(numeros.get(2));
		}
		catch (Exception e)
		{
			assertNotNull("Debería haber arrojado excepción.", e);
		}
		try
		{
			assertNull(numeros.get(3));
		}
		catch (Exception e)
		{
			assertNotNull("Debería haber arrojado excepción.", e);
		}
		assertEquals("Elemento debería ser 200", num200, numeros.get(0));
		assertEquals("Elemento debería ser 300", num300, numeros.get(1));
	}
	/**
	 * Prueba del método set.
	 */
	public void testSet()
	{
		setupEscenario1();
		numeros.add(10);
		numeros.add(100);
		numeros.add(200);
		numeros.add(300);
		Integer num200 = 200;
		Integer num500 = 500;
		assertEquals("Elemento debería ser 200", num200, numeros.get(2));
		numeros.set(500, 2);
		assertEquals("El tamaño debería ser 4", 4, numeros.size());
		assertEquals("Elemento debería ser 500", num500, numeros.get(2));
	}
	/**
	 * ¨Prueba del método clear.
	 */
	public void testClear()
	{
		setupEscenario1();
		for(Integer i= 100; i < 10000; i = i+100)
		{
			numeros.add(i);
		}
		Integer num100 = 100;
		Integer num200 = 200;
		Integer num300 = 300;
		assertEquals("Elemento debería ser 100", num100, numeros.get(0));
		assertEquals("Elemento debería ser 200", num200, numeros.get(1));
		assertEquals("Elemento debería ser 300", num300, numeros.get(2));
		numeros.clear();
		assertEquals("El tamaño debería ser 0", 0, numeros.size());
		try
		{
			assertNull(numeros.get(0));
		}
		catch (Exception e)
		{
			assertNotNull("Debería haber arrojado excepción.", e);
		}
		try
		{
			assertNull(numeros.get(1));
		}
		catch (Exception e)
		{
			assertNotNull("Debería haber arrojado excepción.", e);
		}
		try
		{
			assertNull(numeros.get(2));
		}
		catch (Exception e)
		{
			assertNotNull("Debería haber arrojado excepción.", e);
		}
		try
		{
			assertNull(numeros.get(3));
		}
		catch (Exception e)
		{
			assertNotNull("Debería haber arrojado excepción.", e);
		}
		try
		{
			assertNull(numeros.get(4));
		}
		catch (Exception e)
		{
			assertNotNull("Debería haber arrojado excepción.", e);
		}

		assert(numeros.isEmpty());
	}
	/**
	 * Prueba del método toArray.
	 */
	public void testToArray()
	{
		setupEscenario2();
		Object[] temp = numeros.toArray();
		for(int i = 0; i < temp.length; i++)
		{
			System.out.println(temp[i]);
		}
	}
	
}
